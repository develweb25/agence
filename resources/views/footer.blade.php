<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Material Design -->
<script src="../../dist/js/material.min.js"></script>
<script src="../../dist/js/ripples.min.js"></script>
<script>
$.material.init();
</script>
<!-- DataTables -->

<script type="text/javascript" src="../../bower_components/datatables.net/datatables.min.js"></script>
<script type="text/javascript" src="../../bower_components/datatables.net/SemanticUI-2.2.13/semantic.min.js"></script>

<!-- Select2 -->
<script src="../../bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->

<!-- bootstrap time picker -->
<!-- <script src="../../node_modules/chart_js/dist/Chart.min.js"></script> -->
<script src="../../node_modules/highcharts/code/highcharts.js"></script>
<script src="../../node_modules/highcharts/code/highcharts-3d.js"></script>
<script src="../../node_modules/highcharts/code/modules/exporting.js"></script>
<script src="../../node_modules/highcharts/code/modules/offline-exporting.js"></script>
<!--<script src="../../bower_components/chart.js/Chart.js"></script>--->
<script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="../../plugins/iCheck/icheck.min.js"></script>

<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
$(function () {

    $('#example1').DataTable()

})
$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
            ranges   : {
                'Today'       : [moment(), moment()],
                'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(29, 'days'),
            endDate  : moment()
        },
        function (start, end) {
            $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
        }
    )
    //Date picker
    $('#datepicker').datepicker({
        autoclose: true
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass   : 'iradio_flat-green'
    })

    //Timepicker
    $('.timepicker').timepicker({
        showInputs: false
    });
});
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
function relatorio(opcion) {
    var range = $("#reservation").val();
    //console.log(range);
    var docs = [];
    $(".checkbox input:checkbox:checked").each(function(){
        docs.push($(this).val());
    });
    var parametros ={
        "consultores":docs,
        "rango": range
    };
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : parametros,
        method: "post",
        url  : "{{ route('relatorio') }}",
        success: function(data){
            var result = data.data;
            var datos = {
                "datos": result,
                "consultores": docs
            };
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : datos,
                method: "post",
                url  : "{{ route('tabla') }}",
                success: function(data){
                    $("#result").html(data);
                }
            });
        }
    });
}
function pizza(opcion) {
    var range = $("#reservation").val();
    //console.log(range);
    var docs = [];
    $(".checkbox input:checkbox:checked").each(function(){
        docs.push($(this).val());
    });
    var parametros ={
        "consultores":docs,
        "rango": range
    };
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : parametros,
        method: "post",
        url  : "{{ route('getPizza') }}",
        success: function(data){
            var result = data.data;
            var datos = {
                "datos": result,
                "consultores": docs
            };
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : datos,
                method: "post",
                url  : "{{ route('pizza') }}",
                success: function(data){
                    $("#result").html(data);
                }
            });
        }
    });
}
function grafico(opcion) {
    var range = $("#reservation").val();
    //console.log(range);
    var docs = [];
    $(".checkbox input:checkbox:checked").each(function(){
        docs.push($(this).val());
    });
    var parametros ={
        "consultores":docs,
        "rango": range
    };
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : parametros,
        method: "post",
        url  : "{{ route('getGrafico') }}",
        success: function(data){

            var result = data.data;
            var datos = {
                "datos": result,
                "consultores": docs
            };
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : datos,
                method: "post",
                url  : "{{ route('grafico') }}",
                success: function(data){
                    $("#result").html(data);
                }
            });
        }
    });
}

</script>
</body>
</html>