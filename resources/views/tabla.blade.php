<?php
	$data=$_REQUEST['datos'];
	$consultor=$_REQUEST['consultores'];
?>
@for ($i = 0; $i < count($data); $i++)
<div class="box">
    <div class="box-body">
        <div class="form-group">
            <label>{{$consultor[$i]}}:</label>
        </div>

		<table id="example" class="example1 table table-striped table-bordered table-hover" style="width: 100%">
			<thead>
				<tr>
					<th>Periodo</th>
					<th>Receita Liquida</th>
					<th>Custo Fixo</th>
					<th>Comissão</th>
					<th>Lucro</th>
				</tr>
			</thead>
			<tbody>
				@for ($x = 0; $x < count($data[$i]); $x++)
				<tr>
					<td>{{trim($data[$i][$x]['fecha'])}}</td>
					<td>{{$data[$i][$x]['liquido']}}</td>
					<td>{{$data[$i][$x]['salario']}}</td>
					<td>{{$data[$i][$x]['comissao']}}</td>
					<td>{{$data[$i][$x]['lucro']}}</td>
				</tr>
				@endfor
			</tbody>
		</table>
    </div>
</div>
@endfor
<script >
$(function () {
    $('.select2').select2()
    $('.example1').DataTable({
        'responsive'      : true,
        'paging'      : false,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : false,
        'autoWidth'   : false
    });


  });

</script>